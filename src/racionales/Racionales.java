/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package racionales;

/**
 *
 * @author maryf
 */
public class Racionales {
   
    //Variable que representa un numerador
    int p;
   
    //Variable que representa un denominador
    int q;
  
    /**
     * Constructor por default (sin parametros).
     */
    Racionales(){
        this.p = 1;
        this.q = 1;
    }
    
    /*
    *Constructor con parametros.
    */
    Racionales(int p, int q){
        this.p = p;
        this.q = q;
    }
   
    /*
    *Metodo que asigna el nuevo valor al numerador.
    *@param p, El nuevo valor del numerador.
    */
    private void setNumerador(int p){
        this.p = p;
    }
    
    /*
    *Metodo que asigna el nuevo valor al denominador.
    *@param q, El nuevo valor del denominador.
    */
    private void setDenominador(int q){
        this.q = q;
    }
    
    /*
    *Metod que regresa al valor del numerador.
    *@return El valor del numerador.
    */
    private int getNumerador(){
     return this.p;
    }
    
    /*
    *Metodo que regresa al valor del denominador
    *@return El valor del denominador.
    */
    private int getDenominador(){
        return this.q;
    }
    
    /*
    *Metodo que imprime el numero racional.
    */
    void show(){
        System.out.println(getNumerador() + "/" + getDenominador());
    }
    
    /*
    *Metodo que realiza la multiplicacion de dos numeros racionales.
    *@param r El numero racional con el que multiplicaremos
    *@return El resultado de la multiplicacion.
    */
    Racionales multiplicaRacionales(Racionales r){
        int p = 0;
        int q = 1;
        
        p = getNumerador()*r.getNumerador();
        q = getDenominador()*r.getDenominador();
        
        return (new Racionales(p, q)); 
    }
    Racionales divideRacionales(Racionales r){
        int p = 0;
        int q = 1;
        p = getNumerador()*r.getDenominador();
        q = getDenominador()*r.getNumerador();
        
        return (new Racionales (p, q));
    }
    
    /**
     * @param args the command line arguments
     */
     public static void main(String[] args) {
         
        //Objetos que representaran a dos numeros racionales.
        Racionales r1 = new Racionales(1, 2);
        Racionales r2 = new Racionales (2, 3);
        
        Racionales r3;
        
        Racionales r4;
        
        //Imprimir r1
        System.out.println("R1 es: ");
        r1.show();
        
        //Imprimimos r2
        System.out.println("R2 es: ");
        r2.show();
        
        //En r3 se guarda el resultado de la multiplicacion 
        r3 = r1.multiplicaRacionales(r2);
        
        //Imprimir el resultado.
        System.out.println("El resultado de la multiplicacion es ");
        r3.show();
        
        //En r4 se queda el resultado de la division
        r4 = r1.divideRacionales(r2);
        
        //Imprimir el resultado de la division.
        System.out.println("El resultado de la division es ");
        r4.show();
     }
    
}
